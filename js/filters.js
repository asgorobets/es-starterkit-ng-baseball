'use strict';

/* Filters */

angular.module('ng_baseballFilters', []).filter('normalize_date', function() {
    return function(input) {
      input = new String(input);
      return input.substr(4,2) + '.' + input.substr(6,2) + '.' + input.substr(0,4);
    };
});
