/**
 * Renders the weather status for a city.
 */
var app = angular.module('ng_baseball', ['ng_baseballFilters', 'elasticjs.service']);

    app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'baseball/template/baseball_content',
                controller: 'ListBaseball'
            }).
            when('/:nid', {
                templateUrl: 'baseball/template/baseball_details',
                controller: 'MatchDetails'
            }).
            when('/:nid/edit', {
                templateUrl: 'baseball/template/baseball_form',
                controller: 'MatchForm'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);

    app.controller('ListBaseball', function($scope, ejsResource, $location) {
        $scope.getBaseballListTitle = function() {
            if ($scope.queryTerm === undefined || $scope.queryTerm === '') {
                $scope.listLabel = 'Latest baseball matches';
            } else {
                $scope.listLabel = 'Showing ' + $scope.docs.length + ' results from ' + $scope.total + ' found';
            }
        }
        $scope.getBaseballListTitle();
        var ejs = ejsResource('http://localhost:9200');
        var client = ejs.Request()
            .indices('field_current')
            .types('node');

        $scope.search = function() {
            if ($scope.queryTerm) {
              client.query(ejs.MatchQuery('_all', $scope.queryTerm));
            } else {
              client.query(ejs.MatchAllQuery());
            }
            if ($scope.size) {
              client.size($scope.size);
            }
            // Let's get some facets.
            client.facet(ejs.TermsFacet('type')
                .field('type')
                //.nested('field_home_score')
                .size(10));

            client.fields(['title', 'field_home_team','field_home_score', 'field_visiting_team', 'field_visiting_score', 'field_start_date'])
                  .doSearch(function(data) {
                    $scope.docs = data.hits.hits;
                    $scope.total = data.hits.total;
                    console.log(data);
                    $scope.getBaseballListTitle();
                  });

        };

        $scope.redirectToId = function(path) {
           $location.path(path);
        }
        // Perform initial search.
        $scope.search();
//        // Define a function to process form submission.
//        $scope.change = function() {
//            // Fetch the data from the public API through JSONP.
//            // See http://openweathermap.org/API#weather.
//            var url = 'http://api.openweathermap.org/data/2.5/weather';
//            $http.jsonp(url, { params : {
//                q : $scope.city,
//                units : $scope.units,
//                callback: 'JSON_CALLBACK'
//            }}).
//                success(function(data, status, headers, config) {
//                    $scope.main = data.main;
//                    $scope.wind = data.wind;
//                    $scope.description = data.weather[0].description;
//                }).
//                error(function(data, status, headers, config) {
//                    // Log an error in the browser's console.
//                    $log.error('Could not retrieve data from ' + url);
//                });
//        };

    });

    app.controller('MatchDetails', ['$scope', '$routeParams', 'ejsResource',
    function($scope, $routeParams, ejsResource) {
        var ejs = ejsResource('http://localhost:9200');
        var client = ejs.Request()
            .indices('field_current')
            .types('node')
            .query(ejs.IdsQuery($routeParams.nid))
            .doSearch(function(data) {
               $scope.doc = data.hits.hits[0]._source;
            });
    }]);

    app.controller('MatchForm', ['$scope', '$routeParams', 'ejsResource',
    function($scope, $routeParams, ejsResource) {
        var ejs = ejsResource('http://localhost:9200');
        var client = ejs.Request()
            .indices('field_current')
            .types('node')
            .query(ejs.IdsQuery($routeParams.nid))
            .doSearch(function(data) {
                $scope.doc = data.hits.hits[0]._source;
            });
    }]);