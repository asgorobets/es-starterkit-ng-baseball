<?php
/**
 * @file
 * AngularJS template to render a baseball block.
 */
?>
<div ng-controller="MatchDetails">
  <div class="row">
    <div class="col-md-2 home-team">
      <i class="bb-5x bbclub-{{doc.field_home_team.value}}"></i>
      <h3>{{doc.field_home_team.value}}</h3>
    </div>
    <div class="col-md-4 match-details">
      <h1 style="text-align: center;">{{doc.field_home_score.value}} : {{doc.field_visiting_score.value}}</h1>
    </div>
    <div class="col-md-2 visiting-team">
      <i class="bb-5x bbclub-{{doc.field_visiting_team.value}}"></i>
      <h3>{{doc.field_visiting_team.value}}</h3>
    </div>
  </div>
  <div class="row">
    <a ng-href="#/{{doc.nid}}/edit">Edit</a>
  </div>
</div>