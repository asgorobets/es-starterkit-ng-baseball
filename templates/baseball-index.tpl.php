<?php
/**
 * @file
 * AngularJS template to render a baseball block.
 */
?>
<div class="view-container">
  <div ng-view class="view-frame"></div>
</div>