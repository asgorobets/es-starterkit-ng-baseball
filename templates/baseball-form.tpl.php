<?php
/**
 * @file
 * AngularJS template to render a baseball block.
 */
?>
<div ng-controller="MatchForm">
  <div class="row">
    <div class="col-md-2 home-team">
      <i class="bb-5x bbclub-{{doc.field_home_team.value}}"></i>
      <input ng-model="doc.field_home_team.value" value="{{doc.field_home_team.value}}" />
    </div>
    <div class="col-md-4 match-details">
      <h1 style="text-align: center;">{{doc.field_home_score.value}} : {{doc.field_visiting_score.value}}</h1>
    </div>
    <div class="col-md-2 visiting-team">
      <i class="bb-5x bbclub-{{doc.field_visiting_team.value}}"></i>
      <input ng-model="doc.field_visiting_team.value" value="{{doc.field_visiting_team.value}}" />
    </div>
  </div>
  <div class="row">
    <a ng-href="#/">To listing</a>
  </div>
</div>