<?php
/**
 * @file
 * AngularJS template to render a baseball block.
 */
?>
<div ng-controller="ListBaseball">
  <div class="row">
    <div class="col-md-6">
      <form class="form">
        <div class="form-group col-xs-6">
          <input placeholder="Search here.." type="text" class="form-control" ng-change="search()" ng-model="queryTerm" />
        </div>
        <div class="form-group col-xs-4">
          <input placeholder="limit.." type="text" class="form-control" ng-change="search()" ng- ng-model="size">
        </div>
        <button class="btn btn-primary" ng-click="search()">Search</button>
      </form>
    </div>
  </div>
  <div class="row">
    <h3>{{listLabel}}</h3>
    <div class="col-md-6">
      <table class="table table-hover">
        <tr>
          <th>Title</th>
          <th>Home team</th>
          <th>Home score</th>
          <th>Visiting score</th>
          <th>Visiting team</th>
          <th>Date</th>
        </tr>
        <tr ng-repeat="doc in docs | orderBy:fields.field_start_date.value" ng-click="redirectToId(doc._id)">
          <td>{{doc.fields.title}}</td>
          <td>{{doc.fields.field_home_team.value}}</td>
          <td>{{doc.fields.field_home_score.value}}</td>
          <td>{{doc.fields.field_visiting_score.value}}</td>
          <td>{{doc.fields.field_visiting_team.value}}</td>
          <td>{{doc.fields.field_start_date.value|normalize_date}}</td>
        </tr>
      </table>
    </div>
  </div>
  <div class="col-md-6">

  </div>
</div>